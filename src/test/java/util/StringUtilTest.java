package util;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilTest {

    @Test(expected = IllegalArgumentException.class)
    public void testRepeatNegative(){
        String result = StringUtil.repeat("hi",-1);
        Assert.assertEquals("",result);
    }

    @Test
    public void testRepeatZero(){
        String result = StringUtil.repeat("hi",0);
        Assert.assertEquals("",result);;
    }

    @Test
    public void testRepeatOnce(){
        String result = StringUtil.repeat("hi",1);
        Assert.assertEquals("hi",result);;
    }

    @Test
    public void testRepeatMultipleTimes(){
        String result = StringUtil.repeat("hi",3);
        Assert.assertEquals("hihihi",result);
    }

    //IsEmpty

    @Test
    public void not_empty_string(){
        Assert.assertFalse(StringUtil.isEmpty("any string"));
    }

    @Test
    public void null_empty_string(){
        Assert.assertTrue(StringUtil.isEmpty(null));
    }

    @Test
    public void empty_string(){
        Assert.assertTrue(StringUtil.isEmpty(""));
    }

    @Test
    public void empty_only_spaces_string(){
        Assert.assertTrue(StringUtil.isEmpty("   "));
    }

}
