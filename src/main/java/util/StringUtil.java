package util;

class StringUtil {
    public static String repeat(String str, int times){
        if (times<0){
            throw new IllegalArgumentException("Negative Times not Allowed");
        }

        String result = "";
        for (int i = 0; i < times; i++) {
            result += str;
        }

        return result;
    }

    public static boolean isEmpty(String text){
        return text == null || text.trim().equals("");
    }

}