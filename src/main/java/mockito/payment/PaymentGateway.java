package mockito.payment;

public interface PaymentGateway {

    PaymentResponse requestPayment(PaymentRequest request);
}
